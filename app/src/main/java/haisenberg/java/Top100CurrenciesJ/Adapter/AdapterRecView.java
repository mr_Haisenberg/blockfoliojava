package haisenberg.java.Top100CurrenciesJ.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import haisenberg.java.Top100CurrenciesJ.R;
import haisenberg.java.Top100CurrenciesJ.POJO.GeckoCoin;

public class AdapterRecView extends RecyclerView.Adapter<AdapterRecView.ViewHolder> {

    private List<GeckoCoin> coinsList;
    private AdapterRecView.ClickListener clickListener;

    @Inject
    public AdapterRecView(ClickListener clickListener){

        this.clickListener = clickListener;
        coinsList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rec_view_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GeckoCoin geckoCoin = coinsList.get(position);
        Picasso.with(holder.tvCurrencyIcon.getContext()).load(geckoCoin.getImage()).into(holder.tvCurrencyIcon);
        holder.tvCurrencySym.setText(geckoCoin.getSymbol());
        holder.tvCurrencyName.setText(geckoCoin.getName());
        holder.tvCurrencyMarketCap.setText(String.valueOf(geckoCoin.getMarketCap()));
        holder.tvCurrencyPrice.setText(String.valueOf(geckoCoin.getCurrentPrice()));
    }

    @Override
    public int getItemCount() {
        return coinsList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvCurrencySym;
        private TextView tvCurrencyName;
        private TextView tvCurrencyMarketCap;
        private TextView tvCurrencyPrice;
        private ImageView tvCurrencyIcon;
        private ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCurrencySym = itemView.findViewById(R.id.tvCurrencySym);
            tvCurrencyName = itemView.findViewById(R.id.tvCurrencyName);
            tvCurrencyMarketCap = itemView.findViewById(R.id.tvCurrencyMarketCap);
            tvCurrencyPrice = itemView.findViewById(R.id.tvCurrencyPrice);
            tvCurrencyIcon = itemView.findViewById(R.id.ivCurrencyIcon);
            constraintLayout = itemView.findViewById(R.id.constrLayout);
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   clickListener.lauchIntent(coinsList.get(getAdapterPosition()).getMarketCap(),
                           coinsList.get(getAdapterPosition()).getImage(),
                           coinsList.get(getAdapterPosition()).getMarketCapChangePercentage24h(),
                           coinsList.get(getAdapterPosition()).getAth(),
                           coinsList.get(getAdapterPosition()).getAthChangePercentage(),
                           coinsList.get(getAdapterPosition()).getCirculatingSupply(),
                           coinsList.get(getAdapterPosition()).getTotalSupply());
                }
            });
        }

    }

    public interface ClickListener {
        void lauchIntent(Long marketCap,
                         String icon,
                         Double marketCapChangePercentage24h,
                         Double ath,
                         Double athChangePercentage,
                         Double circulatingSupply,
                         Float totalSupply);
    }
    public void setCoinsList(List<GeckoCoin> data){
        this.coinsList.addAll(data);
        notifyDataSetChanged();
    }
}
