package haisenberg.java.Top100CurrenciesJ;

import android.app.Activity;
import android.app.Application;

import haisenberg.java.Top100CurrenciesJ.Retrofit.JSONPlaceHolderApi;
import haisenberg.java.Top100CurrenciesJ.di.component.ApplicationComponent;
import haisenberg.java.Top100CurrenciesJ.di.component.DaggerApplicationComponent;
import haisenberg.java.Top100CurrenciesJ.di.module.ContextModule;
import retrofit2.Retrofit;

public class App extends Application {
    ApplicationComponent applicationComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this)).build();
        applicationComponent.injectApplication(this);
    }
    public static App get(Activity activity){
        return (App) activity.getApplication();
    }
    public ApplicationComponent getApplicationComponent(){
        return applicationComponent;
    }

}
