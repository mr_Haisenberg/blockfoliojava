package haisenberg.java.Top100CurrenciesJ.Retrofit;

import java.util.List;

import haisenberg.java.Top100CurrenciesJ.POJO.GeckoCoin;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface JSONPlaceHolderApi {
    @GET("coins/markets")
    Observable<List<GeckoCoin>> getPostWithID (@Query("vs_currency") String vsCurrency,
                                               @Query("per_page") Integer perPage,
                                               @Query("sparkline") boolean sparkline,
                                               @Query("order") String order);
}
