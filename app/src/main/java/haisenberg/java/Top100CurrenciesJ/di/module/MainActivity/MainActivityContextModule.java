package haisenberg.java.Top100CurrenciesJ.di.module.MainActivity;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import haisenberg.java.Top100CurrenciesJ.activity.MainActivity;
import haisenberg.java.Top100CurrenciesJ.activity.MainFragment;
import haisenberg.java.Top100CurrenciesJ.di.qualifier.ActivityContext;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ActivityScope;

@Module
public class MainActivityContextModule {
    private MainFragment mainFragment;

    public Context context;

    public MainActivityContextModule(MainFragment mainFragment){
        this.mainFragment = mainFragment;
    }

    @Provides
    @ActivityScope
    public MainFragment providesMainActivity(){
        return mainFragment;
    }

    @Provides
    @ActivityScope
    @ActivityContext
    public Context provideContext(){
        return context;
    }
}
