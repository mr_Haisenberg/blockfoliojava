package haisenberg.java.Top100CurrenciesJ.di.module.MainActivity;

import dagger.Module;
import dagger.Provides;
import haisenberg.java.Top100CurrenciesJ.MVP.MainActivityContract;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ActivityScope;

@Module
public class MAinActivityMvpModule {
    private final MainActivityContract.View mView;


    public MAinActivityMvpModule(MainActivityContract.View mView) {
        this.mView = mView;
    }
    @Provides
    @ActivityScope
    MainActivityContract.View provideView(){
        return mView;
    }
}
