package haisenberg.java.Top100CurrenciesJ.di.component;

import android.content.Context;

import dagger.Component;
import haisenberg.java.Top100CurrenciesJ.App;
import haisenberg.java.Top100CurrenciesJ.Retrofit.JSONPlaceHolderApi;
import haisenberg.java.Top100CurrenciesJ.di.module.ContextModule;
import haisenberg.java.Top100CurrenciesJ.di.module.RetrofitModule;
import haisenberg.java.Top100CurrenciesJ.di.qualifier.ApplicationContext;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ApplicationScope;

@ApplicationScope
@Component(modules = {ContextModule.class, RetrofitModule.class})
public interface ApplicationComponent {

    JSONPlaceHolderApi getJSONPlaceolderApi();

    @ApplicationContext
    Context getContext();

    void injectApplication(App app);
}
