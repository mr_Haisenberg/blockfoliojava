package haisenberg.java.Top100CurrenciesJ.di.module;

import dagger.Module;
import dagger.Provides;
import haisenberg.java.Top100CurrenciesJ.Retrofit.JSONPlaceHolderApi;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ApplicationScope;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RetrofitModule {
    private static final String BASE_URL = "https://api.coingecko.com/api/v3/";

    @Provides
    @ApplicationScope
    JSONPlaceHolderApi getJSONPlaceHolder(Retrofit retrofit){
        return retrofit.create(JSONPlaceHolderApi.class);
    }

    @Provides
    @ApplicationScope
    Retrofit getRetrofit(OkHttpClient okHttpClient){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @ApplicationScope
    OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor){
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @ApplicationScope
    HttpLoggingInterceptor getHttpLoggingInterceptor(){
        return new HttpLoggingInterceptor().
                setLevel(HttpLoggingInterceptor.Level.BODY);
    }
}
