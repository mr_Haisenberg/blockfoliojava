package haisenberg.java.Top100CurrenciesJ.di.component.MainActivity;

import android.content.Context;

import dagger.Component;
import haisenberg.java.Top100CurrenciesJ.activity.MainActivity;
import haisenberg.java.Top100CurrenciesJ.activity.MainFragment;
import haisenberg.java.Top100CurrenciesJ.di.component.ApplicationComponent;
import haisenberg.java.Top100CurrenciesJ.di.module.AdapterModule;
import haisenberg.java.Top100CurrenciesJ.di.module.MainActivity.MAinActivityMvpModule;
import haisenberg.java.Top100CurrenciesJ.di.qualifier.ActivityContext;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ActivityScope;

@ActivityScope
@Component(modules = {AdapterModule.class, MAinActivityMvpModule.class},
        dependencies = ApplicationComponent.class)
public interface MainActivityComponent {

    @ActivityContext
    Context getContext();

    void injectMainActivity(MainFragment mainFragment);
}
