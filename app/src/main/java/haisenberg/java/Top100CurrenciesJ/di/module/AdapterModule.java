package haisenberg.java.Top100CurrenciesJ.di.module;

import dagger.Module;
import dagger.Provides;
import haisenberg.java.Top100CurrenciesJ.Adapter.AdapterRecView;
import haisenberg.java.Top100CurrenciesJ.activity.MainActivity;
import haisenberg.java.Top100CurrenciesJ.activity.MainFragment;
import haisenberg.java.Top100CurrenciesJ.di.module.MainActivity.MainActivityContextModule;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ActivityScope;

@Module(includes = MainActivityContextModule.class)
public class AdapterModule {
    @Provides
    @ActivityScope
    public AdapterRecView getCoinList(AdapterRecView.ClickListener clickListener){
        return new AdapterRecView(clickListener);
    }
    @Provides
    @ActivityScope
    public AdapterRecView.ClickListener getClickListener(MainFragment mainFragment){
        return mainFragment;
    }
}
