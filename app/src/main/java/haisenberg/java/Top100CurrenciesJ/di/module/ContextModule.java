package haisenberg.java.Top100CurrenciesJ.di.module;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import haisenberg.java.Top100CurrenciesJ.di.qualifier.ApplicationContext;
import haisenberg.java.Top100CurrenciesJ.di.scopes.ApplicationScope;

@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context){
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context provideContext(){
        return context;
    }
}
