package haisenberg.java.Top100CurrenciesJ.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Objects;
import haisenberg.java.Top100CurrenciesJ.R;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnRateApp;
    private AdView adView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_about);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        btnRateApp = findViewById(R.id.btnRate);
        btnRateApp.setOnClickListener(this);

        MobileAds.initialize(this, "ca-app-pub-4028811704458362~4923797733");
        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("B1515452D448B2755B38C00F8DE46C10").build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$applicationContext.packageName")));
    }
}
