package haisenberg.java.Top100CurrenciesJ.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import haisenberg.java.Top100CurrenciesJ.Adapter.AdapterRecView;
import haisenberg.java.Top100CurrenciesJ.App;
import haisenberg.java.Top100CurrenciesJ.MVP.MainActivityContract;
import haisenberg.java.Top100CurrenciesJ.MVP.PresenterImpl;
import haisenberg.java.Top100CurrenciesJ.POJO.GeckoCoin;
import haisenberg.java.Top100CurrenciesJ.R;
import haisenberg.java.Top100CurrenciesJ.di.component.ApplicationComponent;
import haisenberg.java.Top100CurrenciesJ.di.component.MainActivity.DaggerMainActivityComponent;
import haisenberg.java.Top100CurrenciesJ.di.component.MainActivity.MainActivityComponent;
import haisenberg.java.Top100CurrenciesJ.di.module.MainActivity.MAinActivityMvpModule;
import haisenberg.java.Top100CurrenciesJ.di.module.MainActivity.MainActivityContextModule;
import haisenberg.java.Top100CurrenciesJ.di.qualifier.ActivityContext;
import haisenberg.java.Top100CurrenciesJ.di.qualifier.ApplicationContext;

public class MainFragment extends Fragment implements AdapterRecView.ClickListener, MainActivityContract.View {
    private RecyclerView recyclerView;
    MainActivityComponent mainActivityComponent;


    @Inject
    public AdapterRecView adapterRecView;

    @Inject
    @ApplicationContext
    public Context mContext;

    @Inject
    @ActivityContext
    public Context activityContext;

    @Inject
    PresenterImpl presenter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_main,container,false);

        ApplicationComponent applicationComponent = App.get(getActivity()).getApplicationComponent();
        mainActivityComponent  = DaggerMainActivityComponent.builder()
                .mainActivityContextModule(new MainActivityContextModule(this))
                .mAinActivityMvpModule(new MAinActivityMvpModule(this))
                .applicationComponent(applicationComponent)
                .build();
        mainActivityComponent.injectMainActivity(this);

        recyclerView = v.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(activityContext));
        recyclerView.setAdapter(adapterRecView);
        presenter.loadData();

        return v;

    }

    @Override
    public void lauchIntent(Long marketCap,
                            String icon,
                            Double marketCapChangePercentage24h,
                            Double ath,
                            Double athChangePercentage,
                            Double circulatingSupply,
                            Float totalSupply){

        //Toast.makeText(mContext,name,Toast.LENGTH_LONG).show();

        Intent intent =new Intent(getActivity(),DetailActivity.class);
        intent.putExtra("marketCap",marketCap);
        intent.putExtra("icon",icon);
        intent.putExtra("marketCapChangePercentage24h",marketCapChangePercentage24h);
        intent.putExtra("ath",ath);
        intent.putExtra("athChangePercentage",athChangePercentage);
        intent.putExtra("circulatingSupply",circulatingSupply);
        intent.putExtra("totalSupply",totalSupply);
        startActivity(intent);

    }


    @Override
    public void showData(List<GeckoCoin> data) {
        adapterRecView.setCoinsList(data);
        adapterRecView.notifyDataSetChanged();
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showComplete() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

}
