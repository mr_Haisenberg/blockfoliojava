package haisenberg.java.Top100CurrenciesJ.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import haisenberg.java.Top100CurrenciesJ.R;

public class DetailActivity extends AppCompatActivity {
    private ImageView ivCurrencyDetailIcon;
    private TextView tvDetailMarketCapRank;
    private TextView tvMarketCapChange;
    private TextView tvATH;
    private TextView tvAthChange;
    private TextView tvCirculatingSupply;
    private TextView tvTotalSupply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_detail);
        ivCurrencyDetailIcon = findViewById(R.id.ivCurrencyDetailIcon);
        tvDetailMarketCapRank = findViewById(R.id.tvDetailMarketCapRank);
        tvMarketCapChange = findViewById(R.id.tvMarketCapChange);
        tvATH = findViewById(R.id.tvATH);
        tvAthChange = findViewById(R.id.tvAthChange);
        tvCirculatingSupply = findViewById(R.id.tvCirculatingSupply);
        tvTotalSupply = findViewById(R.id.tvTotalSupply);

        Intent intent = getIntent();

        Picasso.with(this).load(intent.getStringExtra("icon"))
                .into(ivCurrencyDetailIcon);
        tvDetailMarketCapRank.setText(String.valueOf(intent.getExtras()
                .getLong("marketCap")));
        tvMarketCapChange.setText(String.valueOf(intent.getExtras()
                .getDouble("marketCapChangePercentage24h")));
        tvATH.setText(String.valueOf( intent.getExtras()
                .getDouble("ath")));
        tvAthChange.setText(String.valueOf( intent.getExtras()
                .getDouble("athChangePercentage")));
        tvCirculatingSupply.setText(String.valueOf(intent.getExtras()
                .getDouble("circulatingSupply")));
        tvTotalSupply.setText(String.valueOf(intent.getExtras()
                .getFloat("totalSupply")));

    }
}
