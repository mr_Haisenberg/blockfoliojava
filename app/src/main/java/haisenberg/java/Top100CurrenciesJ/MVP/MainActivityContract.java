package haisenberg.java.Top100CurrenciesJ.MVP;

import java.util.List;

import haisenberg.java.Top100CurrenciesJ.POJO.GeckoCoin;

public interface MainActivityContract {
    interface View{
        void showData(List<GeckoCoin> data);
        void showError(String message);
        void showComplete();
        void showProgress();
        void hideProgress();
    }
    interface Presenter{
        void loadData();
    }
}
