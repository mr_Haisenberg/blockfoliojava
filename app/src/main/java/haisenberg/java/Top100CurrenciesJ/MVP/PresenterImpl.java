package haisenberg.java.Top100CurrenciesJ.MVP;

import java.util.List;

import javax.inject.Inject;

import haisenberg.java.Top100CurrenciesJ.Adapter.AdapterRecView;
import haisenberg.java.Top100CurrenciesJ.POJO.GeckoCoin;
import haisenberg.java.Top100CurrenciesJ.Retrofit.JSONPlaceHolderApi;
import haisenberg.java.Top100CurrenciesJ.activity.DetailActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observer;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PresenterImpl implements MainActivityContract.Presenter {
    @Inject
    JSONPlaceHolderApi jsonPlaceHolderApi;


    MainActivityContract.View mView;

    @Inject
    public PresenterImpl(JSONPlaceHolderApi jsonPlaceHolderApi,MainActivityContract.View mView){
        this.jsonPlaceHolderApi = jsonPlaceHolderApi;
        this.mView = mView;
    }
    @Override
    public void loadData() {

        jsonPlaceHolderApi.getPostWithID("usd",50,false,
                "market_cap_desc").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<GeckoCoin>>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                        mView.showProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError("Error occurred");
                        mView.hideProgress();
                    }

                    @Override
                    public void onNext(List<GeckoCoin> geckoCoins) {
                        mView.showData(geckoCoins);
                    }
                });
    }
}